A tool to read the algo parameters change file, and update the record in the database.

The format of the algo parameters change file:

UpdateAlgoParams.csv

;Machine,Algo,Key,Value
KOSPI,MultiFloatSniperGoldNew,3,0.55