﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;

namespace UpdateAlgoParams
{
    class Program
    {
        static void Main(string[] args)
        {
            string exeDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            StreamReader FileReader;

            FileReader = File.OpenText(exeDir + "\\UpdateAlgoParam.csv");

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=10.9.197.10;User ID=vegasoul;Password=vincent1"; ;
            try
            {
                conn.Open();

                if (FileReader != null)
                {
                    while (FileReader.Peek() != -1)
                    {
                        string line = FileReader.ReadLine();
                        if (!line.Equals("") && !line.StartsWith(";"))
                        {
                            string[] str = line.Split(',');
                            string prod = str[0];
                            string algo = str[1];
                            string configkey = str[2];
                            string configvalue = str[3];
                            string sql = "update [vegasoul].[dbo].[VTS_ExecutionAlgo_Config] set ConfigValue = " + configvalue + " where Product = '" + prod + "' and AlgoName = '" + algo + "' and ConfigKey = " + configkey + ";";

                            SqlCommand cmd = new SqlCommand(sql, conn);
                            cmd.CommandTimeout = 300;
                            int i = cmd.ExecuteNonQuery();
                            Console.WriteLine("Number of lines affected: " + i);
                        }
                    }
                    FileReader.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurs. " + ex.ToString());
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }


        }
    }
}
